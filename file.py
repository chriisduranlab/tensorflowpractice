import tensorflow as tf
from tensorflow.keras.preprocessing import image
import numpy as nip
import pandas as pd

# Cargar modelo preentrenado
model = tf.keras.applications.VGG16(weights='imagenet')

# Cargar y preprocesar imagen de prueba
img_path = 'img2.jpg'  # Ruta de la imagen de prueba
img = image.load_img(img_path, target_size=(224, 224))
x = image.img_to_array(img)
x = nip.expand_dims(x, axis=0)
x = tf.keras.applications.vgg16.preprocess_input(x)

# Realizar predicción
preds = model.predict(x)
decoded_preds = tf.keras.applications.vgg16.decode_predictions(preds, top=3)[0]

# Crear un DataFrame para guardar los resultados
df_results = pd.DataFrame(columns=['Clase', 'Probabilidad'])
for pred in decoded_preds:
    clase = pred[1]
    probabilidad = pred[2] * 100
    df_results = df_results.append({'Clase': clase, 'Probabilidad': probabilidad}, ignore_index=True)

# Guardar los resultados en un archivo CSV
df_results.to_csv('resultados.csv', index=False)

# Imprimir las predicciones
print('Predicciones:')
for pred in decoded_preds:
    print(f'{pred[1]}: {pred[2]*100:.2f}%')
